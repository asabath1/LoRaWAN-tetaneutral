#!/bin/bash

hugo
rsync -avc . -e "ssh -p2222" root@loradmin.tetaneutral.net:/root/docs/LoRaWAN-tetaneutral
ssh -p2222 root@loradmin.tetaneutral.net 'cd /root/docs && docker-compose build && docker-compose up -d'
