#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the loraserver project by Tetaneutral.net
# Copyright (c) 2019 Nicolas Gonzalez.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import requests
import json
import datetime
import time
import urllib
import sys

TELEGRAM_TOKEN  = "" # Create a telegram bot and get its token
TELEGRAM_CHATID = "" # For example : "-164163463"
LORASERVER_URL  = "" # For example : "https://loraserver.test.com"
LORASERVER_USER     = ""
LORASERVER_PASSWORD = ""
ALERT_TIMEOUT   = 60 # Timeout in seconds before sending notifications

token = ""
auth_headers = ""

def get_token():
    global token, auth_headers
    auth_json = {'username': LORASERVER_USER, 'password': LORASERVER_PASSWORD}
    response = requests.post('{}/api/internal/login'.format(LORASERVER_URL), data=json.dumps(auth_json))

    if 'jwt' in response.json():
        token = response.json()['jwt']
        auth_headers={'Grpc-Metadata-Authorization': token}
    else:
        print("Failed to get a jwt token: bad credentials")
        sys.exit(1)

def get_gateway_list():
    global auth_headers
    response = requests.get('{}/api/gateways?limit=1000'.format(LORASERVER_URL), headers=auth_headers)

    if 'error' in response.json():
        get_token()
        response = requests.get('{}/api/gateways?limit=1000'.format(LORASERVER_URL), headers=auth_headers)

    gateways = {}
    for gateway in response.json()['result']:
        gateways[gateway['id']] = {}
        gateways[gateway['id']]['name'] = gateway['name']
    return gateways

def get_gateway_last_seen(gateway_id):
    global auth_headers
    response = requests.get('{}/api/gateways/{}'.format(LORASERVER_URL, gateway_id), headers=auth_headers)

    if 'error' in response.json():
        get_token()
        response = requests.get('{}/api/gateways/{}'.format(LORASERVER_URL, gateway_id), headers=auth_headers)

    last_seen_string = response.json()['lastSeenAt']
    return datetime.datetime.strptime(last_seen_string, '%Y-%m-%dT%H:%M:%S.%fZ')

def send_notification_on_telegram(message):
    url = "https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}".format(TELEGRAM_TOKEN, TELEGRAM_CHATID, urllib.parse.quote(message))
    response = requests.get(url, timeout=10)
    if response.status_code == 200:
        return True
    else:
        return False

def send_notification_up(gateway_name):
    print('[ UP ] {}'.format(gateway_name))
    success = send_notification_on_telegram('[INFO] {} on the road again'.format(gateway_name))
    if not success:
        print('[FAIL] Failed to send telegram notification')

def send_notification_down(gateway_name):
    print('[DOWN] {}'.format(gateway_name))
    success = send_notification_on_telegram('[ALERT] {} is unreacheable'.format(gateway_name))
    if not success:
        print('[FAIL] Failed to send telegram notification')

if __name__ == '__main__':

    gateway_list = {}
    gateway_list_prev = {}

    while True:
        gateway_list_prev = gateway_list
        gateway_list = get_gateway_list()

        for gateway_id in gateway_list.keys():
            lastSeen = get_gateway_last_seen(gateway_id)
            now = datetime.datetime.now()
            last_seen_delta = now - lastSeen
            gateway_list[gateway_id]['timedelta'] = last_seen_delta

            if last_seen_delta > datetime.timedelta(seconds=ALERT_TIMEOUT):
                gateway_list[gateway_id]['state'] = False
                if gateway_id in gateway_list_prev and gateway_list_prev[gateway_id]['state'] == True:
                    send_notification_down(gateway_list[gateway_id]['name'])
            else:
                gateway_list[gateway_id]['state'] = True
                if gateway_id in gateway_list_prev and gateway_list_prev[gateway_id]['state'] == False:
                    send_notification_up(gateway_list[gateway_id]['name'])

        time.sleep(30)
